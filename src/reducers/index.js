import {combineReducers} from 'redux';
import counterReducer  from './counterReducer';
import testReducer  from './testReducer';

import userReducer  from './thunk';


const reducer = combineReducers({
    counter : counterReducer,
    test : testReducer,
    user : userReducer
});

export default reducer;