
let initialState = {
    test : ""
}

const testReducer = (state = initialState, action) => {
    let newState = {...state}
    switch(action.type){
        case 'test' : {
            newState.test = "Souradip";
            return newState;
        }
        default : 
            return state
    }
}

export default testReducer;