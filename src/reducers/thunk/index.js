import {FETCH_FAIL,FETCH_SUCCESS,FETCH_USER} from './../../actions/thunk';

const initialState = {
    isLoading : false,
    users : [],
    error : ''
}

const userReducer = (state = initialState, action) => {

    switch(action.type){
        case FETCH_USER : 
            return {...state, isLoading : true}
        case FETCH_SUCCESS : {
            console.log(action.type)
            return {...state, isLoading : false, users : action.payload}
        }
        case FETCH_FAIL : 
            return {...state, isLoading : false, error : action.payload}
        default : 
            return state;
    }
}

export default userReducer