import React from 'react';
import './App.css';

import {useSelector, useDispatch} from 'react-redux';
import {increment,decrement} from './actions'

import User from './thunk/User'

function App() {

  const counter = useSelector(state => state.counter);
  const dispatch = useDispatch();

  return (
    <div className="App">
      {/* <div>
        {counter.count}
      </div>
        <button onClick = {() => dispatch(increment())}>ADD</button>
        <button onClick = {() => dispatch(decrement())}>SUB</button> */}

        {/* Thunk component */}
        <User/> 
    </div>
  );
}

export default App;
