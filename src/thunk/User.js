import React,{useEffect} from 'react';
import {connect} from 'react-redux';

import {fetchUserAPI} from '../actions/thunk'

function User({user,getUsers}) {

    useEffect(() => {
        getUsers();
    },[]);
    console.log("USER ", user)


    return (
        <div>
                USER
        </div>
    )

   
}

const mapStateToProps = (state,ownProps) => {
    return {
        user : state.user
    }
}

const mapDispatchToProps = (dispatch,ownProps) => {
    return {
        getUsers : () => dispatch(fetchUserAPI())
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(User)
