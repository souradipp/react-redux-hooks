import axios from 'axios';

export const FETCH_USER = "FETCH_USER";
export const FETCH_SUCCESS = "FETCH_SUCCESS";
export const FETCH_FAIL = "FETCH_FAIL";

export const fetchUserSuccess = response => {
    console.log("fetchUserSuccess ", response)

        return {
            type : FETCH_SUCCESS,
            payload : response
        }
}   

export const fetchUserFail = error => {
    return {
        type : FETCH_FAIL,
        payload : error
    }
}

export const fetchUser = () => {
    return {
        type : FETCH_USER
    }
}

export const fetchUserAPI = () => {
    return (dispatch) => {
        dispatch(fetchUser);

        axios.get('https://jsonplaceholder.typicode.com/users')
            .then(res => {
                console.log("RESPONSE ", res)
                const user = res.data;
                dispatch(fetchUserSuccess(user))
            })
            .catch(err => {
                dispatch(fetchUserFail(err.message));
            })
    }
}